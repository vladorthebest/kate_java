public class Uloha1{
    private static int x = 100;
    private static double y = 2.0;

    public double equation1(int a,double b){
        return a-b/(a+b);
    }
    public double equation2(int a,double b){
        return a-b/a+b;
    }
    public void reportResult(double res){
        System.out.println(res);
    }
}
